package tpBanque;

import java.sql.SQLException;
import java.sql.Statement;

public class Compte_courant extends Compte {

	private final Float decouvert_autorise;

	///////////////////////////////////////////////////

	public Compte_courant(Float solde, String proprietaire, Float decouvert_autorise, Statement statement) { //Pour ajouter un compte + l'ajouter dans la BDD
		super(solde, proprietaire, statement);
		this.decouvert_autorise = decouvert_autorise;
		
		try {
			statement.executeUpdate("INSERT INTO compte VALUES ('"+ proprietaire +"', '"+ (float) solde +"', 'courant', '"+ (float) decouvert_autorise +"')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Compte_courant(Float solde, String proprietaire, Float decouvert_autorise) { //Pour ajouter les comptes que l'on recupere de la BDD
		super(solde, proprietaire);
		this.decouvert_autorise = decouvert_autorise;
	}
	
	
	public Float getDecouvert_autorise() {
		return decouvert_autorise;
	}
	
	public Boolean check_decouvert_autorise(Float somme) {
		if (this.solde - somme < -(this.decouvert_autorise)) {
			System.out.println("Erreur: le d�couvert autoris� est atteint.");
			return false;
		}else return true;
	}
	
	public String resumer()
	{
		String result = "--------------------------------------------------";
		result = result + "\nCompte courant de " + this.proprietaire + " :";
		result = result + "\n  D�couvert autoris� : " + this.decouvert_autorise;
		result = result + "\n  Solde: "+this.solde;
		result = result + resumer_operations();
		result = result +"\n-------------------------------------------------- \n\n";
		
		System.out.println(result);
		return result;
	}
	
}
