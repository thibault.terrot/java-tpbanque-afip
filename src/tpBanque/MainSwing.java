package tpBanque;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import com.mysql.jdbc.ResultSetMetaData;

import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;

import javax.swing.border.TitledBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;


import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import com.mysql.jdbc.ResultSetMetaData;

public class MainSwing extends JFrame{
	
	
	private JButton btndebit, btncredit;
	
	JPanel  pan_select_cpt=new JPanel();
	JPanel  pan_info=new JPanel();
	JPanel  pan_detail=new JPanel();
	JPanel  pan_action=new JPanel();
	
	JLabel solde = new JLabel();
    JLabel proprietaire = new JLabel();
    JLabel decouvert = new JLabel();
    
	JTable table_op;
    String[] entetes = {"Date", "Description", "Montant"};
	DefaultTableModel tableModel = new DefaultTableModel(entetes, 0);

    JTextField inputdebcred = new JTextField();
    
    Choice choice = new Choice();
 
    
    private Compte cpt_selectionne;
    private Boolean cpt_select_list_bool = false;
    private Compte cpt_select_list;
	
	private JTextField inputlibelle;
	private final JLabel lblCompte_2 = new JLabel("Compte:");
	public JTextArea error_label = new JTextArea("");
	public JPanel pan_error = new JPanel();
	private Choice choice_cpt = new Choice();
	private Choice choice_del = new Choice();
	
	private List<Compte> liste_compte = new ArrayList<Compte>();
	private final JPanel pan_add_cpt = new JPanel();
	private final JPanel pan_del_cpt = new JPanel();
	private JTextField add_cpt_input_nom;
	private JTextField add_cpt_input_solde;
	private JTextField add_cpt_decouvert_input;
	
	Statement statement = null;
	 
	public static void main(String[] args) throws ClassNotFoundException, SQLException{
		MainSwing tpBanque = new MainSwing();	
		

	}
	
	public static Statement createDatabaseLink(Statement statement) throws ClassNotFoundException, SQLException {
		Connection connection = null ;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_banque" ,"root","");
			statement = connection.createStatement();
			System.out.println("Connection OK");
		}
		catch(ClassNotFoundException | SQLException e){
			System.err.println(e);
		}
		return statement;
	}
	
	public void getAllAcount() throws SQLException {

		ResultSet result = statement.executeQuery("SELECT * FROM compte");
		
        try {
        	ResultSetMetaData rsmd = (ResultSetMetaData) result.getMetaData();
        	int nbCols = rsmd.getColumnCount();
	        boolean encore = result.next();
	         while (encore) {
	            if(result.getString(3).equals("courant")) {
	            	Compte_courant cptCourrantTMP = new Compte_courant(result.getFloat(2), result.getString(1), result.getFloat(4));
	            	liste_compte.add(cptCourrantTMP);
	            	addcptToChoice(cptCourrantTMP);
	            }
	            else if(result.getString(3).equals("epargne_entreprise")) {
	        		Compte_epargne_entreprise cptCourrantTMP = new Compte_epargne_entreprise(result.getFloat(2), result.getString(1), result.getFloat(4));
	            	liste_compte.add(cptCourrantTMP);
	            	addcptToChoice(cptCourrantTMP);
	            }
	            encore = result.next();
	         }
	         result.close();
        }
        catch (SQLException e) {
    	  System.err.println(e);
        }
	}
	
	public void getAllOperation() throws SQLException {

		ResultSet result = statement.executeQuery("SELECT * FROM operation");
		
        try {
        	ResultSetMetaData rsmd = (ResultSetMetaData) result.getMetaData();
        	int nbCols = rsmd.getColumnCount();
	        boolean encore = result.next();
	         while (encore) {
	        	Operation_bancaire opTMP = new Operation_bancaire(result.getFloat(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5));

        		for (Compte temp : liste_compte) {//On ajoute les op�rations de la bdd pour le bon compte
        			if(result.getString(5).equals(temp.getProprietaire())) {
        				temp.liste_operation.add(opTMP);
        			}
        		}
        		
	            encore = result.next();
	         }
	         result.close();
        }
        catch (SQLException e) {
    	  System.err.println(e);
        }
	}
	
	
	public MainSwing() throws ClassNotFoundException, SQLException {
		
		//titre de la fenetre
        super("TPBanque");
		
        choice_cpt.add("Veuillez choisir un compte");
		choice.add("Aucun");
		
		statement = createDatabaseLink(statement);
		getAllAcount();
		getAllOperation();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        
        
        //On creer les panels
        createPanSelectCpt();
        createPanInfo();
        createPanAddCpt();
        createPanDelCpt();
        createPanAction();
        createPanError();
        createPanDetail();
        createMenu();

        
        //On ajoute les panels
        getContentPane().add(pan_detail);
        getContentPane().add(pan_action);
        getContentPane().add(pan_error);
        getContentPane().add(pan_add_cpt);
		getContentPane().add(pan_del_cpt);
        getContentPane().add(pan_info);
        getContentPane().add(pan_select_cpt);
        
        setBounds(100,100,800,800);
        setVisible(true);

	}
	
	public void operation(Statement statement){
		
		//Compte_courant cptCourrantNico = new Compte_courant((float) 0, "Nicolas - Courant", (float) 2000);
		//Compte_epargne_entreprise cptEpargneEntNico = new Compte_epargne_entreprise((float) 0, "Nicolas - Epargne", (float) 0.02);
		//Compte_courant cptCourrantJerem = new Compte_courant((float) 0, "J�r�mie", (float) 500);
		
		//liste_compte.add(cptCourrantNico);
        //liste_compte.add(cptEpargneEntNico);
        //liste_compte.add(cptCourrantJerem);
        //addcptToChoice();
        /*
		cptCourrantNico.crediter((float) 100, "salaire");
		
		//Nicolas fait le plein de la voiture
		cptCourrantNico.debiter((float) 50);
		
		//Il deplace 20e sur son compte epargne
		cptEpargneEntNico.crediter((float) 20, cptCourrantNico);
		
		//KDO de la banque
		cptCourrantNico.crediter((float) 100);
		
		//Il remet les 20e sur son cpt courant
		cptEpargneEntNico.debiter((float) 20, cptCourrantNico);
		
		//Jerem achete un nouveau PC
		cptCourrantJerem.debiter((float) 500);
		
		//Il rembourse ses dettes
		cptCourrantJerem.debiter((float) 200, cptCourrantNico);
		*/
	}
	
	public void addcptToChoice(){
		choice_cpt.add("Veuillez choisir un compte");
		choice.add("Aucun");
		for (Compte temp : this.liste_compte) {
			choice_cpt.add(temp.getProprietaire());
			choice.add(temp.getProprietaire());
			choice_del.add(temp.getProprietaire());
		}
	}
	public void addcptToChoice(Compte cpt){
		choice_cpt.add(cpt.getProprietaire());
		choice.add(cpt.getProprietaire());
		choice_del.add(cpt.getProprietaire());
	
	}
	public void delcptToChoice(Compte cpt){
		choice_cpt.remove(cpt.getProprietaire());
		choice.remove(cpt.getProprietaire());
		choice_del.remove(cpt.getProprietaire());
		
	}
	
	
	public void createMenu(){
		
		JMenuBar menuBar_1 = new JMenuBar();
        menuBar_1.setBounds(0, 0, 782, 26);
        getContentPane().add(menuBar_1);
        
        JMenu mnNewMenu = new JMenu("Espace client");
        menuBar_1.add(mnNewMenu);
        
        JMenuItem mntmNewMenuItem = new JMenuItem("D\u00E9tails des op�rations");
        mntmNewMenuItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		pan_select_cpt.setVisible(true);
        		pan_info.setVisible(true);
        		pan_action.setVisible(false);
                pan_error.setVisible(false);
                pan_detail.setVisible(true);
                pan_add_cpt.setVisible(false);
                pan_del_cpt.setVisible(false);
        	}
        });
        mnNewMenu.add(mntmNewMenuItem);
        
        JMenuItem mntmNewMenuItem_1 = new JMenuItem("Effectuer une op\u00E9ration");
        mntmNewMenuItem_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		pan_select_cpt.setVisible(true);
        		pan_info.setVisible(true);
        		pan_detail.setVisible(false);
                pan_action.setVisible(true);
                pan_error.setVisible(true);
                pan_add_cpt.setVisible(false);
                pan_del_cpt.setVisible(false);
        	}
        });
        mnNewMenu.add(mntmNewMenuItem_1);
        
        JMenu mnNewMenu_1 = new JMenu("Comptes");
        menuBar_1.add(mnNewMenu_1);
        
        JMenuItem mntmNewMenuItem_2 = new JMenuItem("Ajouter un compte");
        mntmNewMenuItem_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		pan_select_cpt.setVisible(false);
        		pan_info.setVisible(false);
        		pan_detail.setVisible(false);
                pan_action.setVisible(false);
                pan_error.setVisible(false);
                pan_add_cpt.setVisible(true);
                pan_del_cpt.setVisible(false);
        	}
        });
        mnNewMenu_1.add(mntmNewMenuItem_2);
        
        JMenuItem mntmNewMenuItem_3 = new JMenuItem("Supprimer un compte");
        mntmNewMenuItem_3.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		pan_select_cpt.setVisible(false);
        		pan_info.setVisible(false);
        		pan_detail.setVisible(false);
                pan_action.setVisible(false);
                pan_error.setVisible(false);
                pan_add_cpt.setVisible(false);
                pan_del_cpt.setVisible(true);
        	}
        });
        mnNewMenu_1.add(mntmNewMenuItem_3);
		
	}
	
	public void createPanSelectCpt() {
        Border border_cpt = BorderFactory.createTitledBorder("Comptes:");

		//ajoute un listener : ici le listener est cette classe
        pan_select_cpt.setBounds(0, 44, 782, 61);
        pan_select_cpt.setLayout(null);
        choice_cpt.addItemListener(new ItemListener() {
        	public void itemStateChanged(ItemEvent arg0) {
        		String item = (String) arg0.getItem();
        		for (Compte temp : liste_compte) {
        			
        			if(temp.getProprietaire() == item) {
        				cpt_selectionne = temp;
        				if(choice_cpt.getItem(0) == "Veuillez choisir un compte") choice_cpt.remove("Veuillez choisir un compte");
        			}
        		}
            	solde.setText(cpt_selectionne.getSolde()+"");
            	proprietaire.setText(cpt_selectionne.getProprietaire());
            	
            	if(cpt_selectionne instanceof Compte_courant) {
                	decouvert.setText(((Compte_courant) cpt_selectionne).getDecouvert_autorise()+"");
            	}else {
            		decouvert.setText("X");
            	}
            	
            	//On cr�er la table des op�rations pour le compte s�l�ctionn�
            	clearTable();
            	for (Operation_bancaire temp : cpt_selectionne.getListe_operation()) {
            		if(temp.getLibelle() != null) addRow(cpt_selectionne.formater_date(temp), temp.getType_mouvement() + " (" + temp.getLibelle()  +")", temp.getMontant());
            		else addRow(cpt_selectionne.formater_date(temp), temp.getType_mouvement(), temp.getMontant());
            	}
        	}
        });

        choice_cpt.setBounds(21, 20, 737, 22);
        
        pan_select_cpt.add(choice_cpt);
        pan_select_cpt.setBorder(border_cpt);
	}
	
	public void createPanInfo() {
        Border border_info = BorderFactory.createTitledBorder("Informations g�n�rales:");
        
        JLabel lsolde = new JLabel("Solde: ");
        JLabel lproprietaire = new JLabel("Proprietaire: ");
        JLabel ldecouvert = new JLabel("D�couvert autoris�: ");
        
		pan_info.setBounds(32, 118, 724, 71);
        pan_info.setBorder(border_info);
        GroupLayout gl_pan_info = new GroupLayout(pan_info);
        gl_pan_info.setHorizontalGroup(
        	gl_pan_info.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_pan_info.createSequentialGroup()
        			.addGroup(gl_pan_info.createParallelGroup(Alignment.LEADING)
        				.addComponent(lsolde, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
        				.addGroup(gl_pan_info.createSequentialGroup()
        					.addGap(47)
        					.addComponent(solde, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)))
        			.addGap(67)
        			.addComponent(lproprietaire, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
        			.addComponent(proprietaire, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
        			.addGap(37)
        			.addComponent(ldecouvert)
        			.addGap(14)
        			.addComponent(decouvert, GroupLayout.PREFERRED_SIZE, 128, GroupLayout.PREFERRED_SIZE)
        			.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        gl_pan_info.setVerticalGroup(
        	gl_pan_info.createParallelGroup(Alignment.LEADING)
        		.addGroup(gl_pan_info.createSequentialGroup()
        			.addContainerGap()
        			.addGroup(gl_pan_info.createParallelGroup(Alignment.LEADING)
        				.addComponent(lsolde, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        				.addComponent(solde, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        				.addComponent(lproprietaire, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        				.addComponent(proprietaire, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        				.addComponent(ldecouvert, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        				.addComponent(decouvert, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
        			.addContainerGap(31, Short.MAX_VALUE))
        );
        pan_info.setLayout(gl_pan_info);
	}
	
	public void createPanAddCpt() {
        Border border_add_cpt = BorderFactory.createTitledBorder("Ajouter un compte:");

		pan_add_cpt.setBounds(31, 51, 724, 689);
        pan_add_cpt.setLayout(null);
        
        add_cpt_input_nom = new JTextField();
        add_cpt_input_nom.setBounds(542, 48, 153, 20);
        pan_add_cpt.add(add_cpt_input_nom);
        add_cpt_input_nom.setColumns(10);
        
        JLabel add_cpt_nom = new JLabel("Nom du compte");
        add_cpt_nom.setBounds(423, 51, 97, 14);
        pan_add_cpt.add(add_cpt_nom);
        
        JLabel add_cpt_type_cpt = new JLabel("Type de compte");
        add_cpt_type_cpt.setBounds(50, 51, 102, 14);
        pan_add_cpt.add(add_cpt_type_cpt);
        
        JLabel add_cpt_solde = new JLabel("Solde");
        add_cpt_solde.setBounds(50, 116, 46, 14);
        pan_add_cpt.add(add_cpt_solde);
        
        JLabel add_cpt_decouvert = new JLabel("D\u00E9couvert autoris\u00E9");
        add_cpt_decouvert.setBounds(418, 116, 114, 14);
        pan_add_cpt.add(add_cpt_decouvert);
        
        add_cpt_input_solde = new JTextField();
        add_cpt_input_solde.setBounds(171, 113, 153, 20);
        pan_add_cpt.add(add_cpt_input_solde);
        add_cpt_input_solde.setColumns(10);
        
        add_cpt_decouvert_input = new JTextField();
        add_cpt_decouvert_input.setBounds(542, 113, 153, 20);
        pan_add_cpt.add(add_cpt_decouvert_input);
        add_cpt_decouvert_input.setColumns(10);
        pan_add_cpt.setBorder(border_add_cpt);
        
        
        
        Choice add_cpt_choice = new Choice();
        add_cpt_choice.setBounds(171, 48, 153, 20);
        add_cpt_choice.add("Compte courant");
        add_cpt_choice.add("Compte �pargne entreprise");
        pan_add_cpt.add(add_cpt_choice);
        
        JLabel success_add_cpt = new JLabel("Compte ajout\u00E9 avec succ\u00E9s!");
		success_add_cpt.setForeground(Color.GREEN.darker());
		success_add_cpt.setBounds(279, 150, 241, 14);
		success_add_cpt.setVisible(false);
		pan_add_cpt.add(success_add_cpt);
        
        JButton btnNewButton = new JButton("Ajouter le compte");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		
        		if(add_cpt_choice.getSelectedItem().matches("Compte courant")) {//On ajoute un cpt courant
        			Compte_courant cpt_temp = new Compte_courant((float) Float.parseFloat(add_cpt_input_solde.getText()), add_cpt_input_nom.getText(), (float) Float.parseFloat(add_cpt_decouvert_input.getText()), statement);
        			liste_compte.add(cpt_temp);
        			addcptToChoice(cpt_temp);
        		}
        		else if(add_cpt_choice.getSelectedItem().matches("Compte �pargne entreprise")) { //On ajoute un cpt epargne entreprise
        			Compte_epargne_entreprise cpt_temp = new Compte_epargne_entreprise((float) Float.parseFloat(add_cpt_input_solde.getText()), add_cpt_input_nom.getText(), (float) 0, statement);
        			liste_compte.add(cpt_temp);
        			addcptToChoice(cpt_temp);
        		}
        		success_add_cpt.setVisible(true);

        	}
        });
		
		
		btnNewButton.setBounds(273, 205, 153, 23);
        pan_add_cpt.add(btnNewButton);
        
        pan_add_cpt.setVisible(false);

	}
	
	public void createPanDelCpt() throws SQLException {		
		
        Border border_del_cpt = BorderFactory.createTitledBorder("Supprimer un compte:");

		pan_del_cpt.setBounds(32, 51, 724, 675);
		

		pan_del_cpt.setLayout(null);
        
		JLabel del_cpt = new JLabel("Compte � supprimer");
		del_cpt.setBounds(50, 51, 115, 14);
		pan_del_cpt.add(del_cpt);
		
		
		
		choice_del.setBounds(171, 48, 153, 20);
		
		pan_del_cpt.add(choice_del);
		
        JLabel success_del_cpt = new JLabel("Compte supprim� avec succ\u00E9s!");
        success_del_cpt.setForeground(Color.GREEN.darker());
        success_del_cpt.setBounds(279, 150, 241, 14);
        success_del_cpt.setVisible(false);
        pan_del_cpt.add(success_del_cpt);
		
		JButton btnNewButton = new JButton("Supprimer le compte");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
    			String item = (String) choice_del.getSelectedItem();
				for (Compte temp : liste_compte) {
					
					if(temp.getProprietaire() == item) {
						cpt_selectionne = temp;
					}
				}
    			liste_compte.remove(cpt_selectionne);//Pour supprimer de la liste
    			delcptToChoice(cpt_selectionne);//Pour supprimer de la liste
    			try {//Pour supprimer de la BDD
					statement.executeUpdate("DELETE from compte where proprietaire ='" + cpt_selectionne.getProprietaire() +"';");
					success_del_cpt.setVisible(true);
				} catch (SQLException e) {
					System.err.println("Ce compte n'est pas d�fini dans la BDD");;
				}
			}
		});
		btnNewButton.setBounds(273, 205, 153, 23);
		pan_del_cpt.add(btnNewButton);
        
        pan_del_cpt.setBorder(border_del_cpt);
        
		pan_del_cpt.setVisible(false);

	}
	
	
	
	public void createPanAction() {
        Border border_action = BorderFactory.createTitledBorder("Actions:");

		btndebit = new JButton("D�biter");
        btndebit.setBounds(296, 31, 182, 89);
        btncredit = new JButton("Ajouter");
        btncredit.setBounds(505, 31, 182, 89);
        
        btndebit.addActionListener(new ButtonDebit());
        btncredit.addActionListener(new ButtonCredit());
        pan_action.setBounds(32, 202, 724, 147);
        pan_action.setLayout(null);
        inputdebcred.setBounds(73, 24, 195, 31);
        
        pan_action.add(inputdebcred);
        pan_action.add(btndebit);
        pan_action.add(btncredit);
        pan_action.setBorder(border_action);
        
        JLabel lblNewLabel = new JLabel("Montant:");
        lblNewLabel.setBounds(12, 31, 56, 16);
        pan_action.add(lblNewLabel);
        
        JLabel lblCompte = new JLabel("Libelle:");
        lblCompte.setBounds(12, 67, 56, 16);
        pan_action.add(lblCompte);
        
        inputlibelle = new JTextField();
        inputlibelle.setBounds(73, 60, 195, 31);
        pan_action.add(inputlibelle);
        
        choice.addItemListener (new ChangeCptList());
        choice.setBounds(73, 102, 195, 31);
        pan_action.add(choice);
        lblCompte_2.setBounds(12, 102, 56, 16);
        
        pan_action.add(lblCompte_2);
        pan_action.setVisible(false);
	}
	
	public void createPanError() {
		pan_error.setBorder(new TitledBorder(null, "Erreurs:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        pan_error.setBounds(32, 362, 724, 378);
        pan_error.setLayout(null);
        
        error_label.setBounds(12, 23, 687, 342);
        error_label.setForeground(Color.RED);
        pan_error.add(error_label);
        pan_error.setVisible(false);
	}
	
	public void createPanDetail() {
		pan_detail.setBounds(32, 202, 724, 538);
        pan_detail.setLayout(new BorderLayout());
        Border border_cpt3 = BorderFactory.createTitledBorder("Op�rations:");
        pan_detail.setBorder(border_cpt3);
        
        //Table
        table_op = new JTable(tableModel) {
        	public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
        	{
    		    Component c = super.prepareRenderer(renderer, row, column);
        		String desc = (String) getModel().getValueAt(row, 1);
        		if (desc.startsWith("debit ")) c.setForeground(Color.RED.darker());
        		else if(desc.startsWith("erreur")) {
        			c.setForeground(Color.BLACK);
        		}
        		else c.setForeground(Color.GREEN.darker());
    		    return c;
    		}
        };
        pan_detail.add(table_op, BorderLayout.NORTH);
    }
	
	
	public void clearTable()
	{
		tableModel.setRowCount(0);
	}
	
	public void addRow(String date, String description, Float montant)
	{
		tableModel.addRow(new Object[]{date, description, montant});
	}
	
	public class ChangeCptList implements ItemListener
    {
		@Override
		public void itemStateChanged(ItemEvent arg0) {
			if (arg0.getStateChange() == ItemEvent.SELECTED) {
				
				String item = (String) arg0.getItem();
				cpt_select_list_bool = false;
        		for (Compte temp : liste_compte) {
        			if(temp.getProprietaire() == item) {
        				cpt_select_list_bool = true;
			        	cpt_select_list = temp;
		        	}			          
		  		}
		    }
		}
    }
     
     public class ButtonDebit implements ActionListener
     {
         public void actionPerformed(ActionEvent e)
         {
        	 try
        	 {
        		 Boolean ok = true;
        		 if(cpt_select_list_bool) {//Un compte est s�lectionn� dans le select
        			 ok = cpt_selectionne.debiter(Float.parseFloat(inputdebcred.getText()), cpt_select_list, statement);
        		 }else{
        			 if(inputlibelle.getText().isEmpty()) {
            			 ok = cpt_selectionne.debiter(Float.parseFloat(inputdebcred.getText()), statement);
            		 }else {
            			 ok = cpt_selectionne.debiter(Float.parseFloat(inputdebcred.getText()), inputlibelle.getText(), statement);
            		 }
        		 }
        		 if(!ok) {
        			 error_label.setForeground(Color.RED);
        			 error_label.setText("\nLe d�couvert autoris� est atteint pour le compte de "+ cpt_selectionne.getProprietaire() +"."+ error_label.getText());
        		 }else {
        			 error_label.setForeground(Color.BLACK);
        			 error_label.setText("\nD�bit de "+ Float.parseFloat(inputdebcred.getText()) +" effectu�e avec succes sur le compte de "+ cpt_selectionne.getProprietaire() +"."+ error_label.getText());
        			 solde.setText(cpt_selectionne.getSolde()+"");
        		 }
        	 }
        	 catch(NullPointerException | NumberFormatException e1) {
        		 error_label.setForeground(Color.RED);
        		 error_label.setText("\nVeuillez choisir un montant et un compte � d�biter."+ error_label.getText());
        	 }
         }
     }
     
     public class ButtonCredit implements ActionListener
     {
         public void actionPerformed(ActionEvent e)
         {
        	 try
        	 {
        		 Boolean ok = true;
        		 if(cpt_select_list_bool) {//Un compte est s�lectionn� dans le select
        			 ok = cpt_selectionne.crediter(Float.parseFloat(inputdebcred.getText()), cpt_select_list, statement);
        		 }else {
        			 if(inputlibelle.getText().isEmpty()) {
        				 cpt_selectionne.crediter(Float.parseFloat(inputdebcred.getText()), statement);
            		 }else{
            			 cpt_selectionne.crediter(Float.parseFloat(inputdebcred.getText()), inputlibelle.getText(), statement);
            		 } 
        		 }
        		 if(!ok) {
        			 error_label.setForeground(Color.RED);
        			 error_label.setText("\nLe d�couvert autoris� est atteint pour le compte de "+ cpt_select_list.getProprietaire() +"."+ error_label.getText());
        		 }else {
        			 error_label.setForeground(Color.BLACK);
        			 error_label.setText("\nCr�dit de "+ Float.parseFloat(inputdebcred.getText()) +" effectu�e avec succes sur le compte de "+ cpt_selectionne.getProprietaire() +"."+ error_label.getText());
        			 solde.setText(cpt_selectionne.getSolde()+"");
        		 }
        		 
        	 }
        	 catch(NullPointerException | NumberFormatException e1) {
        		 error_label.setText("\nVeuillez choisir un montant et un compte � cr�diter."+ error_label.getText());
        	 }
         }
     }
}
