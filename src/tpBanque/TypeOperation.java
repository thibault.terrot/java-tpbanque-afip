package tpBanque;

public enum TypeOperation {

	CREDIT("credit"),
	DEBIT("debit"),
	ERRDEBIT("erreur-debit"),
	ERRCREDIT("erreur-credit");
	
	private final String type;
	
	TypeOperation(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
