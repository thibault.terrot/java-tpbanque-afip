package tpBanque;

import java.sql.SQLException;
import java.sql.Statement;

public class Compte_epargne_entreprise extends Compte{

	private final Float taux_dabondement;
	
	public Compte_epargne_entreprise(Float solde, String proprietaire, Float taux_dabondement, Statement statement) {//Pour ajouter un compte + l'ajouter dans la BDD
		super(solde, proprietaire, statement);
		if (taux_dabondement <= 1 && taux_dabondement >= 0) {
			this.taux_dabondement = taux_dabondement;
		}
		else {
			this.taux_dabondement = (float) 0;
			System.out.println("Le taux d'abondement (" + taux_dabondement + ") n'�tant pas compris entre 0 et 1 � �t� mis � 0.");
		}
		
		try {
			statement.executeUpdate("INSERT INTO compte VALUES ('"+ proprietaire +"', '"+ (float) solde +"', 'epargne_entreprise', '0')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	public Compte_epargne_entreprise(Float solde, String proprietaire, Float taux_dabondement) {//Pour ajouter les comptes que l'on recupere de la BDD
		super(solde, proprietaire);
		if (taux_dabondement <= 1 && taux_dabondement >= 0) {
			this.taux_dabondement = taux_dabondement;
		}
		else {
			this.taux_dabondement = (float) 0;
			System.out.println("Le taux d'abondement (" + taux_dabondement + ") n'�tant pas compris entre 0 et 1 � �t� mis � 0.");
		}
	}
}
