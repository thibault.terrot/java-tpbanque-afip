package tpBanque;

import java.sql.Connection;
 
import java.sql.Statement;
 
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;

import com.mysql.jdbc.ResultSetMetaData;
	  
	 
public class Sql extends JFrame{
	 
	private static javax.swing.JList<String> list;
	 
	public static void main(String args[]) throws ClassNotFoundException, SQLException
	 
	{
		Connection connection = null ;
		Statement statement = null ;
	
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			 
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/afip" ,"root","");
			 
			statement = connection.createStatement();
			 
			System.out.println("Connection Established");
			 
		 
		}catch(ClassNotFoundException | SQLException e){
			System.err.println(e);
		}
		
		
		ResultSet result = statement.executeQuery("SELECT * FROM langue");		
      try {
         ResultSetMetaData rsmd = (ResultSetMetaData) result.getMetaData();
         int nbCols = rsmd.getColumnCount();
         boolean encore = result.next();

         while (encore) {

            for (int i = 1; i <= nbCols; i++)
               System.out.print(result.getString(i) + " ");
            System.out.println();
            encore = result.next();
         }

         result.close();
      } catch (SQLException e) {
    	  System.err.println(e);
      }
	}
}

