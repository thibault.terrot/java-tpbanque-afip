package tpBanque;
import java.time.LocalTime;
import java.util.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class Operation_bancaire {

	private Float montant = (float) 0;	
	private TypeOperation type_mouvement = null;
	private String libelle;
	private LocalDate date;
	private LocalTime time;
	private String proprietaire;
	
	public Operation_bancaire(Float montant, TypeOperation type_mouvement, Statement statement, String proprietaire) {//Pour ajouter l'operation + l'ajouter dans la BDD
		super();
		this.montant = montant;
		this.type_mouvement = type_mouvement;
		
		this.date = LocalDate.now();
		this.time = LocalTime.now();
		
		this.proprietaire = proprietaire;
		System.out.println(type_mouvement);
		try {
			statement.executeUpdate("INSERT INTO operation VALUES ('"+ (float) montant +"', '"+ type_mouvement +"', ' X ', '"+ this.date +" "+ this.time.withNano(0)+"', '"+ proprietaire +"')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Operation_bancaire(Float montant, TypeOperation type_mouvement, String libelle, Statement statement, String proprietaire) {//Pour ajouter l'operation avec libelle + l'ajouter dans la BDD
		super();
		this.montant = montant;
		this.type_mouvement = type_mouvement;
		
		this.date = LocalDate.now();
		this.time = LocalTime.now();
		
		this.proprietaire = proprietaire;
		
		try {
			statement.executeUpdate("INSERT INTO operation VALUES ('"+ (float) montant +"', '"+ type_mouvement +"', '"+ libelle +"', '"+ this.date +" "+ this.time.withNano(0) +"', '"+ proprietaire +"')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Operation_bancaire(Float montant, String type_mouvement, String libelle, String datetime, String proprietaire) {//Pour ajouter les operations que l'on recupere de la BDD
		super();
		this.montant = montant;
		
		if(libelle.equals("X")) this.libelle = "";
		else this.libelle = libelle;
			
		if(type_mouvement.equals("CREDIT"))this.type_mouvement = TypeOperation.CREDIT;
		else if(type_mouvement.equals("ERRCREDIT"))this.type_mouvement = TypeOperation.ERRCREDIT;
		else if(type_mouvement.equals("DEBIT"))this.type_mouvement = TypeOperation.DEBIT;
		else if(type_mouvement.equals("ERRDEBIT"))this.type_mouvement = TypeOperation.ERRDEBIT;
		
		
		this.date = LocalDate.parse(datetime.split(" ")[0]);
		this.time = LocalTime.parse(datetime.split(" ")[1]);
		
		this.proprietaire = proprietaire;
	}
	
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public Float getMontant() {
		return montant;
	}
	public void setMontant(Float montant) {
		this.montant = montant;
	}
	public String getType_mouvement() {
		return type_mouvement.getType();
	}
	public void setType_mouvement(TypeOperation type_mouvement) {
		if ((type_mouvement == TypeOperation.DEBIT) || (type_mouvement == TypeOperation.CREDIT)) this.type_mouvement = type_mouvement;
		if ((type_mouvement == TypeOperation.ERRCREDIT) || (type_mouvement == TypeOperation.ERRDEBIT)) {
			this.type_mouvement = type_mouvement;
			this.libelle = type_mouvement.getType();
		}
		else System.out.println("Le type de mouvement est inconnu.");
	}
	
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
		
	}
}
