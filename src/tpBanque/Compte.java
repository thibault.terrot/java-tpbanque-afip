package tpBanque;

import java.time.LocalTime;
import java.lang.reflect.Array;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter; // Import the DateTimeFormatter class

import java.util.ArrayList;
import java.util.List;

public class Compte {

	protected Float solde = (float) 0;
	protected String proprietaire = null;
	protected List<Operation_bancaire> liste_operation;

	///////////////////////////////////////////////////

	
	public Float getSolde() {
		return solde;
	}

	public void setSolde(Float solde) {
		this.solde = solde;
	}


	
	public String getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}
	
	

	public List<Operation_bancaire> getListe_operation() {
		return (List<Operation_bancaire>) liste_operation;
	}
	
	
	///////////////////////////////////////////////////
	
	public Compte(Float solde, String proprietaire, Statement statement) {//Pour ajouter un compte + l'ajouter dans la BDD
		super();
		this.solde = solde;
		this.proprietaire = proprietaire;
		this.liste_operation = new ArrayList<Operation_bancaire>();
		
		try {
			statement.executeUpdate("INSERT INTO compte VALUES ('"+ proprietaire +"', '"+ (float) solde +"', 'normal', '"+ (float) 0 +"')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Compte(Float solde, String proprietaire) {//Pour ajouter les comptes que l'on recupere de la BDD
		super();
		this.solde = solde;
		this.proprietaire = proprietaire;
		this.liste_operation = new ArrayList<Operation_bancaire>();
	}
	
	public void crediter(Float somme, Statement statement)
	{
		Operation_bancaire op = new Operation_bancaire(somme, TypeOperation.CREDIT, statement, this.proprietaire);
		this.liste_operation.add(op);
		this.solde += somme;
		try {
			statement.executeUpdate("UPDATE compte SET solde = "+ this.solde +" WHERE proprietaire = '"+ this.proprietaire +"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void crediter(Float somme, String description, Statement statement)
	{
		Operation_bancaire op = new Operation_bancaire(somme, TypeOperation.CREDIT, description, statement, this.proprietaire);
		op.setLibelle(description);
		this.liste_operation.add(op);
		this.solde += somme;
		try {
			statement.executeUpdate("UPDATE compte SET solde = "+ this.solde +" WHERE proprietaire = '"+ this.proprietaire +"'");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public Boolean crediter(Float somme, Compte cpt, Statement statement)
	{		
		Boolean ok = true;
		Operation_bancaire op;

		//Si c'est un cpt courant et le decouvert est atteint
		if(cpt instanceof Compte_courant && !((Compte_courant) cpt).check_decouvert_autorise(somme)) 
		{
			op = new Operation_bancaire(somme, TypeOperation.ERRCREDIT, cpt.proprietaire, statement, this.proprietaire);
		    op.setType_mouvement(TypeOperation.ERRCREDIT);
		    ok = false;
		}
		else 
		{
			op = new Operation_bancaire(somme, TypeOperation.CREDIT, cpt.proprietaire, statement, this.proprietaire);
			cpt.debiter(somme, this.proprietaire, statement);
			this.solde += somme;
			try {
				statement.executeUpdate("UPDATE compte SET solde = "+ this.solde +" WHERE proprietaire = '"+ this.proprietaire +"'");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		op.setLibelle(cpt.proprietaire);
		this.liste_operation.add(op);
		
		return ok;
	}
	
	public Boolean debiter(Float somme, Statement statement)
	{
		Boolean ok = true;
		Operation_bancaire op;
		
		//Si c'est un cpt courant et le decouvert est atteint
		if(this instanceof Compte_courant && !((Compte_courant) this).check_decouvert_autorise(somme))
		{
			op = new Operation_bancaire(somme, TypeOperation.ERRDEBIT, statement, this.proprietaire);
			op.setType_mouvement(TypeOperation.ERRDEBIT);
			op.setLibelle(this.proprietaire);
			ok = false;
		}
		else {
			op = new Operation_bancaire(somme, TypeOperation.DEBIT, statement, this.proprietaire);
			this.solde -= somme;
			try {
				statement.executeUpdate("UPDATE compte SET solde = "+ this.solde +" WHERE proprietaire = '"+ this.proprietaire +"'");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		this.liste_operation.add(op);
		return ok;
	}
	
	public Boolean debiter(Float somme, String description, Statement statement)
	{
		Boolean ok = true;
		Operation_bancaire op; 
		
		//Si c'est un cpt courant et le decouvert est atteint
		if(this instanceof Compte_courant && !((Compte_courant) this).check_decouvert_autorise(somme))
		{
			op = new Operation_bancaire(somme, TypeOperation.ERRDEBIT, description, statement, this.proprietaire);
			op.setLibelle(description);
			op.setType_mouvement(TypeOperation.ERRDEBIT);
			op.setLibelle(this.proprietaire);
			ok = false;
		}
		else {
			op = new Operation_bancaire(somme, TypeOperation.DEBIT, description, statement, this.proprietaire);
			op.setLibelle(description);
			this.solde -= somme;
			try {
				statement.executeUpdate("UPDATE compte SET solde = "+ this.solde +" WHERE proprietaire = '"+ this.proprietaire +"'");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		this.liste_operation.add(op);
		return ok;
	}
	
	
	public Boolean debiter(Float somme, Compte cpt, Statement statement)
	{
		Boolean ok = true;
		Operation_bancaire op; 
		
		//Si c'est un cpt courant et le decouvert est atteint
		if(this instanceof Compte_courant && !((Compte_courant) this).check_decouvert_autorise(somme))
		{
			op = new Operation_bancaire(somme, TypeOperation.ERRDEBIT, cpt.proprietaire, statement, this.proprietaire);
			op.setType_mouvement(TypeOperation.ERRDEBIT);
			ok = false;
		}
		else {
			op = new Operation_bancaire(somme, TypeOperation.DEBIT, cpt.proprietaire, statement, this.proprietaire);
			cpt.crediter(somme, this.proprietaire, statement);
			this.solde -= somme;
			try {
				statement.executeUpdate("UPDATE compte SET solde = "+ this.solde +" WHERE proprietaire = '"+ this.proprietaire +"'");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		op.setLibelle(cpt.proprietaire);
		this.liste_operation.add(op);
		return ok;
	}
	
	public String resumer()
	{
		String result = "--------------------------------------------------";
		result = result + "\nCompte de " + this.proprietaire + " :";
		result = result + "\n  Solde: "+this.solde;
		result = result + resumer_operations();
		result = result +"\n--------------------------------------------------\n\n";
		System.out.println(result);
		return result;
	}
	
	public String formater_date(Operation_bancaire temp) {
		DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		String formattedDate = temp.getDate().format(dateFormat);
		String formattedTime = temp.getTime().format(timeFormat);
		
		return formattedDate + " " + formattedTime;
		
	}
	public String resumer_operations() {
		
		String result = "\n\n  Opérations: ";
		for (Operation_bancaire temp : this.liste_operation) {
			
			if(temp.getLibelle() != null) {
				
				result = result + "\n    " + formater_date(temp) + " " + temp.getType_mouvement() + " (" + temp.getLibelle()  +") : " + temp.getMontant();
			}
			else {
				result = result + "\n    " + formater_date(temp) + " " + temp.getType_mouvement() + " : " + temp.getMontant();
			}
        }
		return result;
	}
	
	public ArrayList<ArrayList<String>> resumer_operations_swing() {
		DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		ArrayList<ArrayList<String>> result = null;
		
		for (Operation_bancaire temp : this.liste_operation) {
			ArrayList<String> list = null;
			
			
			if(temp.getLibelle() != null) {
				list.add(formater_date(temp));
				list.add(temp.getType_mouvement() + " (" + temp.getLibelle()  +")");
				list.add(temp.getMontant() +"");
			}
			else {
				list.add(formater_date(temp));
				list.add(temp.getType_mouvement());
				list.add(temp.getMontant() +"");
			}
			result.add(list);
        }
		return result;
	}
	
	
}
