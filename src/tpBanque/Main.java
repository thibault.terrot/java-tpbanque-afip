package tpBanque;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

public class Main {

	public static void main(String[] args) {
		
		
		Compte_courant cptCourrantNico = new Compte_courant((float) 0, "Nicolas - Courant", (float) 2000);
		Compte_epargne_entreprise cptEpargneEntNico = new Compte_epargne_entreprise((float) 0, "Nicolas - Epargne", (float) 0.02);
		Compte_courant cptCourrantJerem = new Compte_courant((float) 0, "Jérémie", (float) 500);
		
		System.out.println("Opérations:");
		
		//Nicolas touche son salaire de 100e
		/*
		 * cptCourrantNico.crediter((float) 100, "salaire");
		 * 
		 * //Nicolas fait le plein de la voiture cptCourrantNico.debiter((float) 50);
		 * 
		 * //Il deplace 20e sur son compte epargne cptEpargneEntNico.crediter((float)
		 * 20, cptCourrantNico);
		 * 
		 * //KDO de la banque cptCourrantNico.crediter((float) 100);
		 * 
		 * //Il remet les 20e sur son cpt courant cptEpargneEntNico.debiter((float) 20,
		 * cptCourrantNico);
		 * 
		 * //Jerem achete un nouveau PC cptCourrantJerem.debiter((float) 500);
		 * 
		 * //Il rembourse ses dettes cptCourrantJerem.debiter((float) 200,
		 * cptCourrantNico);
		 */
		
		System.out.println("\n\nAffichage des comptes: \n");
		
	    cptCourrantNico.resumer();
		cptEpargneEntNico.resumer();
		cptCourrantJerem.resumer();
	}

}
